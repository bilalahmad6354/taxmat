<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receipts', function (Blueprint $table) {
            //

            $table->string('subtotal')->default('');
            $table->string('total')->default('');
            $table->string('tax')->default('');
            $table->string('merchant_address')->default('');
            $table->string('merchant_name')->default('');
            $table->string('merchant_ph')->default('');
            $table->string('tran_date')->default('');
            $table->string('tran_time')->default('');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipts', function (Blueprint $table) {
            
            $table->dropColumn('subtotal');
            $table->dropColumn('total');
            $table->dropColumn('tax');
            $table->dropColumn('merchant_address');
            $table->dropColumn('merchant_name');
            $table->dropColumn('merchant_ph');
            $table->dropColumn('tran_date');
            $table->dropColumn('tran_time');
        });
    }
}
