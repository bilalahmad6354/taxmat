<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Hash;

use App\User;

class AuthController extends Controller
{
    //

    public function register (Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    
        if ($validator->fails())
        {
            $response = ['success' => false, 'isLogin' => false, 'message' => $validator->errors()->all()];

            return response($response, 200);
        }
    
        $request['password']=Hash::make($request['password']);
        $user = User::create($request->toArray());
    
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token , 'isLogin' => true, 'message' => "You have been logged in successfully.", 'user' => $user];
    
        return response($response, 200);
    
    }

    public function login (Request $request) {

        $user = User::where('email', $request->email)->first();
    
        if ($user) {
    
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token , 'isLogin' => true, 'message' => "You have been logged in successfully.", 'user' => $user];
                return response($response, 200);
            } else {
                $response = ['success' => true, 'isLogin' => false , 'message' => "Password is wrong."];
                return response($response, 200);
            }
    
        } else {
            $response = ['success' => true, 'isLogin' => false, 'message' => "Email is not registerd yet."];
            return response($response, 200);
        }
    
    }

    public function logout (Request $request) {

        $token = $request->user()->token();
        $token->revoke();
    
        $response = 'You have been succesfully logged out!';
        return response($response, 200);
    
    }
    
}
