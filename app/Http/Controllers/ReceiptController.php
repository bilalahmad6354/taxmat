<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receipt;
use Validator;
use Auth;
class ReceiptController extends Controller
{
    protected $guarded = ['id'];
    protected $upload_path = "uploads/abcdef.jpg";
    protected $web_url = "http://taxmat.howfindmissing.com/public/attachments/";

    public function postReceipt(Request $request){

        

//         echo json_encode(array('response' => 'Image uploaded successfully.'));

// exit();
        $input = $request->all();
        // // $title = $input['title'];

        // file_put_contents($this->upload_path, base64_decode($input['image']));
        
        
        $file = base64_decode($input['image']);

     
        $folderName = '/attachments/';
        $safeName = $this->random_alphanumeric(15) .'.'.'png';
        $destinationPath = public_path() . $folderName;
      
        $success = file_put_contents($destinationPath .$safeName, $file);

        // $image = new Image();
        // $image->photo = 'uploads/aaa.png';
        // $image->name = 'aaa.png';
        // $result = $image->save();
        // if ($result){
        //     return response()->json('yes');
        // }
        // else{
        //     return response()->json('no');
        // }
        
        $response = $this->hitMicrosoftApi($this->web_url .$safeName);
        echo json_encode(array('response' => $response, 'img_url' => $this->web_url .$safeName));

        exit();
    }


    public function hitMicrosoftApi($url){
        
      
        

        $curl_handle=curl_init();
        curl_setopt($curl_handle,CURLOPT_URL,'https://taxmate.cognitiveservices.azure.com/formrecognizer/v1.0-preview/prebuilt/receipt/asyncBatchAnalyze');
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,60);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl_handle, CURLOPT_POST,1);
        curl_setopt($curl_handle, CURLOPT_VERBOSE,1);
        
        curl_setopt($curl_handle,CURLOPT_HEADER,1);
        curl_setopt($curl_handle,CURLOPT_HTTPHEADER,array('Content-Type:application/json','Ocp-Apim-Subscription-Key:e16cfa1b134f4a25a1ecfa71e364416d'));

        $some_data = array(
        'url'=>$url,
            );


            $postdata = json_encode($some_data);

            curl_setopt($curl_handle,CURLOPT_POSTFIELDS,$postdata);



            $buffer = curl_exec($curl_handle);

            $header_size = curl_getinfo($curl_handle, CURLINFO_HEADER_SIZE);
            $header = substr($buffer, 0, $header_size);
            $body = substr($buffer,$header_size);
            $arr_response = $this->get_headers_from_curl_response($buffer);

            print_r($arr_response);
            exit();
            
            $process_url = $arr_response['Operation-Location'];
            $operation_id = substr($process_url, strrpos($process_url, '/') + 1);
            
            sleep(10); 
            
            
        curl_close($curl_handle);
            
              
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,"https://taxmate.cognitiveservices.azure.com/formrecognizer/v1.0-preview/prebuilt/receipt/operations/" . $operation_id);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,60);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Ocp-Apim-Subscription-Key:e16cfa1b134f4a25a1ecfa71e364416d'));


        $buffer = curl_exec($ch);
        curl_close($ch);

        return json_decode($buffer);
        
    
    }
    
    
    
      function random_alphanumeric($length) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345689';
    $my_string = '';
    for ($i = 0; $i < $length; $i++) {
      $pos = mt_rand(0, strlen($chars) -1);
      $my_string .= substr($chars, $pos, 1);
    }
    return $my_string;
  }
    
    
    
    function get_headers_from_curl_response($response)
    {
        $headers = array();

        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $header_text) as $i => $line)
            if ($i === 0)
                $headers['http_code'] = $line;
            else
            {
                list ($key, $value) = explode(': ', $line);

                $headers[$key] = $value;
            }

        return $headers;
    }


    function addReceiptEntry(Request $request){

        $validator = Validator::make($request->all(), [
            'subtotal' => 'required',
            'total' => 'required',
            'tax' => 'required',
            'img_path' => 'required',
            
            'mer_name' => 'required',
            'mer_add' => 'required',
            'mer_ph' => 'required', 
            
            'tran_date' => 'required',
            'tran_time' => 'required',
            'extracted_text' => 'required'
        ]);
    
        if ($validator->fails())
        {
            $response = ['success' => true, 'isOperationSuccessfull' => false, 'message' => $validator->errors()->all()];

            return response($response, 200);
        }


        $input = $request->all();
        $subtotal =  $input['subtotal'];
        $total  =  $input['total'];
        $tax  = $input['tax'];
        $mer_name =  $input['mer_name'];
        $mer_add = $input['mer_add'];
        $mer_ph = $input['mer_ph'];
        $tran_date = $input['tran_date'];
        $tran_time = $input['tran_time'];
        $user_id = $request->user()->id;
        $img_path = $input['img_path'];
        $extracted_text = $input['extracted_text'];


        $receipt_reg = Receipt::create([

            'subtotal' => $subtotal,
            'total' =>  $total,
            'tax' => $tax,
            'merchant_name' => $mer_name,
            'merchant_address' =>  $mer_add,
            'merchant_ph' => $mer_ph,
            'tran_time' =>  $tran_time,
            'tran_date' =>  $tran_date,
            'user_id' => $user_id,
            'path' => $img_path,
            'text' => $extracted_text
            
        ]);


        $response = ['success' => true, 'isOperationSuccessfull' => true, 'receipt' => $receipt_reg];

        return response($response, 200);


    }


    public function getReceipts(){


        $receipt_reg = Receipt::where('user_id', Auth::user()->id)->get();

        $response = ['success' => true, 'isOperationSuccessfull' => true, 'receipt' => $receipt_reg];
                return response($response, 200);
    }

}
