<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

   // public routes
   Route::post('/login', 'AuthController@login')->name('login.api');
   Route::post('/register', 'AuthController@register')->name('register.api');

   // private routes
   Route::middleware('auth:api')->group(function () {
       Route::get('/logout', 'Api\AuthController@logout')->name('logout');
       Route::post('/addReceiptEntry', 'ReceiptController@addReceiptEntry')->name('addreceipt.api');
       Route::get('/getReceipts', 'ReceiptController@getReceipts')->name('addreceipt.api');
   });


Route::post('postReceipt', 'ReceiptController@postReceipt');
Route::get('test', 'ReceiptController@test');
